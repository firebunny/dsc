# Changelog Discord Server Central Server Bot 

##Version 4.0 - 01/04/2018

### Added
- servers -> Displays servers own by a member(bot must be added to their server)
- rules -> Displays discord server central server rules
- partners -> Displays DSC partnered servers
- [Automated] When posting an ad you will now get the server owner role


### Changed
- bump output
- bump cooldown message now includes (last bump time/date, last bump by)
- admute now deletes all advertisements made by you

### Removed
- Alot of useless stuff